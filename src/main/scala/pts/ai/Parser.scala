package pts.ai

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer

class Parser(source: String, private[this] val handlers : Seq[Handler]) {

  private[this] val lexer = new Lexer(source)
  
  private var state : State = StNone
    
  def parse = {
    handler.start
    while(lexer.next.nonEmpty)
      state.parse(lexer.current.get)
    handler.end
  }
  
  trait State {
    def parse : PartialFunction[Token, Unit]
  }
  
  private[this] object StNone extends State {
    def parse0 : PartialFunction[Token, Unit] = {
        case T_word("class") => {
            val `type` = lexer.next(true) 
            val name = lexer.next(true)
          if(`type`.isEmpty || name.isEmpty || lexer.next(true).isEmpty)
            throw new RuntimeException(s"parser error: class define error ${lexer.pos}")
          if(!(lexer.current contains T_colon))
            throw new RuntimeException(s"parser error: waiting colon ':' ${lexer.pos}")
          val parent = parentName
          handler.classBegin(name.get.toString, parent, `type`.get.toString.toInt)
          state = StClass
        }
        case T_word(name) => {
          val value = lexer.next(true)
          if(value.isEmpty)
            throw new RuntimeException(s"parser error: global value ${lexer.pos}")
          handler.global(name, value.get.toString) 
        }
    }
    
    def parse = parse0.orElse(data)
    
    private[this] def parentName : String = {
      if(lexer.next(true) contains T_lbracket) {
        if(!(lexer.next(true) contains T_word("null")))
          throw new RuntimeException(s"parser error: class define. parent name error. ${lexer.pos}")
        if(!(lexer.next(true) contains T_rbracket))
          throw new RuntimeException(s"parser error: class define. parent name error. ${lexer.pos}")
        "null"
      } else {
        if(lexer.isEmpty || !lexer.current.get.isInstanceOf[T_word])
          throw new RuntimeException(s"parser error: class define. parent name error. ${lexer.pos}")
        lexer.current.get.toString()
      }
    }
  }
  
  private[this] object StClass extends State {
    def parse0 : PartialFunction[Token, Unit] = {
      case T_word("class_end") =>
        handler.classEnd
        state = StNone
      case T_word("handler") =>
        val id = lexer.next(true)
        val lines = lexer.next(true)
        handler.handlerBegin(id.get.toString().toInt, lines.get.toString.toInt)
        state = StHandler
      case T_word("parameter_define_begin") =>
        handler.parameterBegin
        state = StParameter
      case T_word("property_define_begin") =>
        handler.propertyBegin
        state = StProperty
    }
    
    def parse = parse0.orElse(data)
  }
  
  private[this] object StHandler extends State {
    def parse0 : PartialFunction[Token, Unit] = {
        case T_word("handler_end") => {
          handler.handlerEnd
          state = StClass
        }
        case T_word("variable_begin") => {
          handler.variableBegin
          state = StVariable
        }
        case T_word(name) => {
          if(name.startsWith("S")) {
            if(!(lexer.next(true) contains T_dot))
              throw new RuntimeException(s"parser error: string definition error waiting '.' except ${lexer.current} ${lexer.pos}")
            if(!(lexer.next(true).get.isInstanceOf[T_string]))
              throw new RuntimeException(s"parser error: string definition error. Waiting t_string ${lexer.current} ${lexer.pos}")
            handler.string(name.substring(1).toInt, lexer.current.get.toString)
          } else if(name.startsWith("L")) {
            handler.label(name.substring(1).toInt)
          } else {
            val n = Parser.instructions.get(name)
            if(n.isEmpty)
              throw new RuntimeException(s"parser error: unknown instruction ${name}")
            handler.instruction(name, (for(i <- 0 until n.get) yield lexer.next(true).get.toString).toArray)//TODO check op value
          }
        }
    }
    
    def parse = parse0.orElse(data)
  }
  
  private[this] object StVariable extends State {
    def parse0 : PartialFunction[Token, Unit] = {
      case T_word("variable_end") => {
        handler.variableEnd
        state = StHandler
      }
      case T_string(value) => handler.variable(value)
    }
    
    def parse = parse0.orElse(data)
  }
  
  private[this] object StParameter extends State {
    def parse0 : PartialFunction[Token, Unit] = {
      case T_word("parameter_define_end") => {
        handler.parameterEnd
        state = StClass
      }
      case T_word(_type) => {
        //check pattern type
        //TODO remove must check if end of line and skip value, if not exists.
        _type match {
          case "int" | "float" | "string" => {
            val name = lexer.next(true)    
            val value = lexer.next(true)
            if(name.isEmpty || value.isEmpty)
              throw new RuntimeException(s"parser error: parameter define error ${lexer.pos}")
            handler.parameter(name.get.toString, _type, value.get.toString)
          }
          case "waypointstype" | "waypointdelaystype" => {
            val name = lexer.next(true)
            handler.parameter(name.get.toString, _type, "")
          }
          case _ => sys.error("unknown type " + _type)
        }

      }
    }
    
    def parse = parse0.orElse(data)
  }
  
  private[this] object StProperty extends State {
    def parse0 : PartialFunction[Token, Unit] = {
      case T_word("property_define_end") =>
        handler.propertyEnd
        state = StClass
      case T_word("telposlist_begin") => {
        val name = lexer.next(true)
        if(name.isEmpty)
          sys.error(s"parser error: telposlist name ${lexer.pos}")
        handler.telposlistBegin(name.get.toString)
        
        @tailrec
        def parseX : Unit = {
          lexer.next(1).get match {
            case T_lbrace => {
              lexer.next
              handler.telpos(parseObject)
              parseX
            }
            case T_word("telposlist_end") =>
              lexer.next
              handler.telposlistEnd
            case T_space | T_newline => 
              lexer.next
              parseX
          }
        }
        parseX
      }
      case T_word("buyselllist_begin") => {
        val name = lexer.next(true)
        if(name.isEmpty)
          sys.error(s"parser error: buyselllist name ${lexer.pos}")
        handler.buyselllistBegin(name.get.toString)
        
        @tailrec
        def parseX : Unit = {
          lexer.next(1).get match {
            case T_lbrace => {
              lexer.next
              handler.item(parseObject)
              parseX
            }
            case T_word("buyselllist_end") =>
              lexer.next
              handler.buyselllistEnd
            case T_space | T_newline => 
              lexer.next
              parseX
          }
        }
        parseX
      }
    }
    
    def parse = parse0.orElse(data)
  }
  
  private[this] def parseObject : Array[Any] = {
    val list = new ArrayBuffer[Any]
    @tailrec
    def parse : Unit = {
      lexer.current.get match {
        case T_string(value) => { list += lexer.current.get.data ; lexer.next ; parse }
        case T_number(value) => { list += lexer.current.get.data ; ; lexer.next ; parse  }
        case T_real(value) => { list += lexer.current.get.data ; lexer.next ; parse  }
        case T_newline | T_space => lexer.next ; parse 
        case T_semicolon => lexer.next ; parse 
        case T_lbrace => lexer.next ; parse
        case T_rbrace => lexer.next
        case x => sys.error(s"parser error: ${x} ${lexer.pos}")
      }
    }
    parse
    list.toArray
  }
  
  private[this] 
  val handler = new Handler {
      def start =
        handlers.foreach { _.start }
      
      def end =
        handlers.foreach { _.end }
      
      def comment(value: String) =
        handlers.foreach { _.comment(value) }
      
      def global(name: String, value: String) =
        handlers.foreach { _.global(name, value) } 
      
      def classBegin(name: String, parent: String, `type`: Int) =
        handlers.foreach { _.classBegin(name, parent, `type`) }
      
      def classEnd =
        handlers.foreach { _.classEnd }
      
      def parameterBegin =
        handlers.foreach { _.parameterBegin }
      
      def parameter(name: String, `type`: String, value: String) =
        handlers.foreach { _.parameter(name, `type`, value) }
      
      def parameterEnd =
        handlers.foreach { _.parameterEnd }
      
      def handlerBegin(id: Int, lines: Int) =
        handlers.foreach { _.handlerBegin(id, lines) }
      
      def instruction(name: String, op: Array[String]) =
        handlers.foreach { _.instruction(name, op) }
      
      def label(n: Int) =
        handlers.foreach { _.label(n) }
      
      def string(n: Int, value: String) =
        handlers.foreach { _.string(n, value) }
      
      def handlerEnd =
        handlers.foreach { _.handlerEnd }
      
      def variableBegin =
        handlers.foreach { _.variableBegin }
      
      def variable(name: String) =
        handlers.foreach { _.variable(name) }
      
      def variableEnd =
        handlers.foreach { _.variableEnd }
      
      def propertyBegin =
        handlers.foreach { _.propertyBegin }
      
      def propertyEnd =
        handlers.foreach { _.propertyEnd }
      
      def buyselllistBegin(name: String) =
        handlers.foreach { _.buyselllistBegin(name) }
      
      def item(data: Array[Any]) =
        handlers.foreach { _.item(data) }
      
      def buyselllistEnd =
        handlers.foreach { _.buyselllistEnd }
      
      def telposlistBegin(name: String) =
        handlers.foreach { _.telposlistBegin(name) }
      
      def telpos(data: Array[Any]) =
        handlers.foreach { _.telpos(data) }
      
      def telposlistEnd =
        handlers.foreach { _.telposlistEnd }
      
      def newLine =
        handlers.foreach { _.newLine }
  }
  
  private[this] def data : PartialFunction[Token, Unit] = {
    case T_comment(value) => handler.comment(value)
    case T_newline => handler.newLine
    case T_space => //handler.space(value)
    case token => new RuntimeException(s"syntax error: ${token} ${lexer.pos}")
  }
}

object Parser {
  def apply(source: String, handler: Handler) : Parser = 
    Parser(source, handler :: Nil)
  
  def apply(source: String, handlers: Seq[Handler]) =
    new Parser(source, handlers)
  
  private[ai] val instructions = Map(
    "push_event" -> 0,
    "push_const" -> 1,
    "push_string" -> 1,
    "push_parameter" -> 1,
    "push_property" -> 1,
    "push_reg_sp" -> 0,
    "shift_sp" -> 1,
    "add" -> 0,
    "mod" -> 0,
    "sub" -> 0,
    "mul" -> 0,
    "div" -> 0,
    "negate" -> 0,
    "equal" -> 0,
    "greater" -> 0,
    "greater_equal" -> 0,
    "less_equal" -> 0,
    "or" -> 0,
    "and" -> 0,
    "not" -> 0,
    "less" -> 0,
    "not_equal" -> 0,
    "branch_false" -> 1,
    "branch_true" -> 1,
    "jump" -> 1,
    "exit_handler" -> 0,
    "fetch_i" -> 0,
    "fetch_d" -> 0,
    "fetch_f" -> 0,
    "fetch_i4" -> 0,
    "assign" -> 0,
    "assign4" -> 0,
    "call_super" -> 0,
    "func_call" -> 1,
    "add_string" -> 1,
    "bit_and" -> 0,
    "bit_or" -> 0
  );
}

trait Handler {
  
  def start
  
  def end
  
  def comment(value: String)
  
  def global(name: String, value: String) 
  
  def classBegin(name: String, parent: String, `type`: Int)
  
  def classEnd
  
  def parameterBegin
  
  def parameter(name: String, `type`: String, value: String)
  
  def parameterEnd
  
  def handlerBegin(id: Int, lines: Int)
  
  def instruction(name: String, op: Array[String])
  
  def label(n: Int)
  
  def string(n: Int, value: String)
  
  def handlerEnd
  
  def variableBegin
  
  def variable(name: String)
  
  def variableEnd
  
  def propertyBegin
  
  def propertyEnd
  
  def buyselllistBegin(name: String)
  
  def item(data: Array[Any])
  
  def buyselllistEnd
  
  def telposlistBegin(name: String)
  
  def telpos(data: Array[Any])
  
  def telposlistEnd
  
  def newLine
}

class DefaultHandler extends Handler {
  
  def start = { }
  
  def end = { }
  
  def comment(value: String) = { }
  
  def global(name: String, value: String) = {}
  
  def classBegin(name: String, parent: String, `type`: Int) = {}
  
  def classEnd = { } 
  
  def parameterBegin = { }
  
  def parameter(name: String, `type`: String, value: String) = { }
  
  def parameterEnd = { }
  
  def handlerBegin(htype: Int, lines: Int) = { }
  
  def instruction(name: String, op: Array[String]) = { }
  
  def label(n: Int) = { } 
  
  def string(n: Int, value: String) = { }
  
  def handlerEnd = { }
  
  def variableBegin = { }
  
  def variable(name: String) = { }
  
  def variableEnd = { }
  
  def propertyBegin = { }
  
  def propertyEnd = { }
  
  def buyselllistBegin(name: String) = { }
  
  def item(data: Array[Any]) = { }
  
  def buyselllistEnd = { }
  
  def telposlistBegin(name: String) = { }
  
  def telpos(data: Array[Any]) = { }
  
  def telposlistEnd = { }
  
  def newLine = { }
}
