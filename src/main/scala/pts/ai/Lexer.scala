package pts.ai

import java.io.FileReader
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.FileInputStream
import java.util.Arrays
import scala.collection.mutable.ArrayStack
import scala.annotation.tailrec

trait Token {
  def data : Any = ""
}

case class T_word(value: String) extends Token {
  override def data = value
  override def toString = value
}

case class T_number(value: Int) extends Token {
  override def data = value
  override def toString = value.toString
}

case class T_real(value: Float) extends Token {
  override def data = value
  override def toString = value.toString
}

case class T_string(value: String) extends Token {
  override def data = '"' + value + '"'
  override def toString = value
}

case class T_comment(value: String) extends Token {
  override def toString = value
}

case object T_colon extends Token//:
case object T_semicolon extends Token//;
case object T_lbracket extends Token//(
case object T_rbracket extends Token//)
case object T_dot extends Token//.
case object T_minus extends Token//-
case object T_lbrace extends Token//{
case object T_rbrace extends Token//}
case object T_slash extends Token//'/'
case object T_newline extends Token//\n
case object T_space extends Token

class Lexer(private[this] val source: String) extends Traversable[Token] {
  private val BufferSize = 6;
  
  private[this] val in = new BufferedReader(new InputStreamReader(new FileInputStream(source), "UTF-8"))
   
   private var line = 1
   
   private var peek = ' '
   
   private var eof = false
   
   private var start = false
   
   private val buffer = new Array[Option[Token]](BufferSize*2 + 1)
   for(i <- 0 until buffer.length)
     buffer(i) = Option.empty[Token]
  
   private[this] def read : Boolean = {
     if(!eof) {
       val c = in.read
       if(c == -1)
         eof = true
       else
         peek = c.toChar
       !eof
     } else
       false
   }
   
   private[this] def shift = {
     for(i <- 0 until (buffer.length - 1))
       buffer(i) = buffer(i + 1)
     if(eof)
       buffer(buffer.length - 1) = Option.empty[Token]
     else
       buffer(buffer.length - 1) = scan
   }
   
   def next : Option[Token] = {
     if(!eof && buffer(BufferSize).isEmpty) {
       for(i <- 0 to BufferSize)
         buffer(BufferSize + i) = scan
     }
     cur = buffer(BufferSize)
     shift
     cur
   }
   
   def prev : Option[Token] = 
     prev(1)
     
   def prev(n: Int) = 
     buffer(BufferSize - n)
     
   def next(n: Int) =
     buffer(BufferSize + n)
    
   private[this]
   var cur = Option.empty[Token]
   
   //skip whitespace
   def next(skip: Boolean) : Option[Token] = {
     do {
       next
     } while(skip && cur.nonEmpty && ((cur contains T_space) || (cur contains T_newline)))
     cur
   }
   
   def current = cur
   
   def pos = s"pos[${line}]"
   
   private[this] def scan : Option[Token] = {
     if(!start) { read ; start = true }
     if(peek == 65279)
         read
     if(!eof) {
       var str = new StringBuilder
       peek match {
         case ' ' | '\t' | '\r' => {
           do {
             str += peek
           } while(read && (peek == ' ' || peek == '\t' || peek == '\r'))
           Some(T_space)
         }
         case '\n' => { line = line + 1 ; read ; Some(T_newline) }
         case '/' => {
           if(read && peek == '/') {
             while(read && peek != '\n')
               if(peek != '\r')
                 str += peek
             Some(T_comment(str.toString))
           } else
             Some(T_slash)
         }
         case ':' => { read ; Some(T_colon) }
         case '(' => { read ; Some(T_lbracket) }
         case ')' => { read ; Some(T_rbracket) }
         case '{' => { read ; Some(T_lbrace) }
         case '}' => { read ; Some(T_rbrace) }
         case '.' => { read ; Some(T_dot) }
         case ';' => { read ; Some(T_semicolon) }
         case '"' => {
           while(read && peek != '"')
             str += peek
           if(peek != '"')
             println("lexem error: dont closed string")
           else
             read
           Some(T_string(str.toString))
         }
         case '-' => {
           str += peek
           if(read) {
             peek match {
               case _ if peek.isDigit => {
                 var isFloat = false;
                 do {
                   str += peek
                   if(peek == '.')
                     isFloat = true;
                 } while(read && (peek.isDigit || peek == '.'))
                 if(isFloat)
                   Some(T_real(str.toFloat))
                 else
                   Some(T_number(str.toInt))                 
               }
               case _ => { println("wrong after minus " + peek); None }
             }
           } else
             Some(T_minus)
         }
         case _ if peek.isLetter || peek == '_' => {
           do {
             str += peek  
           } while(read && (peek.isLetterOrDigit || peek == '_'))
           Some(T_word(str.toString()))
         }
         case _ if peek.isDigit => {
           var isFloat = false;
           do {
             str += peek
             if(peek == '.')
               isFloat = true;
           } while(read && (peek.isDigit || peek == '.'))
           if(isFloat)
             Some(T_real(str.toFloat))
           else
             Some(T_number(str.toInt))
         }
         
         case _ => { println("unknown " + peek.toInt) ; None }
       }
     } else
       None
   }
   
   
   def foreach[U](f: Token => U) {
     @tailrec
     def r : Unit = {
       val token = scan
       if(token.nonEmpty) {
         f(token.get)
         r
       }
     }
     r
   }
}