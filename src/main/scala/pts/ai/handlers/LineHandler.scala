package pts.ai.handlers

import pts.ai.DefaultHandler

/**
 * Test line count in handler
 */
class LineHandler extends DefaultHandler {
  private var lines = 0
  private var count = 0
  private var handler = 0
  private var className = ""
  
  override
  def classBegin(name: String, parent: String, `type`: Int) =
    className = name
    
  override
  def handlerBegin(id: Int, lines: Int) = {
    handler = id
    this.lines = lines;
  }
  
  override
  def string(index: Int, value: String) = 
    inc
    
  override
  def label(index: Int) =
    inc
  
  override
  def instruction(name: String, op: Array[String]) =
    inc
  
  @inline
  private[this]
  def inc =
    count = count + 1
  
  override
  def handlerEnd = {
    if(count != lines)
      println(s"wrong line count ${className} ${handler} ${count}:${lines}")
    count = 0
    lines = 0
  }
}