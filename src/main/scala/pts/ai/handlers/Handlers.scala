package pts.ai.handlers

import pts.ai.DefaultHandler
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.io.Writer
import scala.collection.mutable.HashMap
import scala.collection.mutable.TreeSet

object Handlers {
  sealed abstract class PrinterHandler extends DefaultHandler {
    def print(out: OutputStream) : Unit =
      print(new OutputStreamWriter(out))
      
    def print(out: Writer)
  }
  
  val instructions = new PrinterHandler {
    val list = new TreeSet[String]
    
    override
    def start = 
      list.clear()
      
    override
    def instruction(name: String, op: Array[String]) =
      list += name
      
    def print(out: Writer) = {
      for(i <- list)
        out.append(s"${i}\n")
      out.flush
    }
  }
  
  /** scan handlers. 
   *  Get names from comments
   */
  val hnames = new PrinterHandler {
    val names = new HashMap[Integer, HashMap[Integer, String]]
    
    //class group, handler index
    //if store true next comment put in to names map
    private var (group, index, store) = (0, 0, false)
    
    override
    def classBegin(name: String, parent: String, g: Int) = {
      group = g
    }
      
    override
    def handlerBegin(index: Int, lines: Int) = {
      store = true
      this.index = index
    }
    
    override
    def comment(content: String) =
      if(store) {
        names.getOrElseUpdate(group, { new HashMap[Integer, String] }).put(index, content.trim)
        store = false
      }
    
    override
    def start =
      names.clear
      
    override
    def print(out: Writer) = {
      for {
        (g, map) <- names
        index <- {
          out.append(s"#GROUP ${g}\n")
          map.keySet.toSeq.sorted
        }
      } out.append(s"${index} ${map(index)}\n")
      out.flush
    }    
  }
  
  /** Get function names from comment
   */
  val fnames = new PrinterHandler {
    //contains funcion's names
    val names = new HashMap[Int, String]
    //findex - function index from operand of func_call instruction 
    //store - boolean flag, if true then next comment store as function name with index in findex var
    var (findex, store) = (0, false)
    
    override
    def start =
      names.clear
      
    override
    def instruction(name: String, op: Array[String]) =
      if(name.equals("func_call")) {
        findex = op(0).toInt 
        store = true
      }
    
    override
    def comment(content: String) =
      if(store) {
        var x = content.trim 
        if(x.trim.startsWith("func["))
          names.put(findex, x.substring(5, x.length() - 1))
        store = false
      }
    
    //handle if after funct_call no comment
    override
    def newLine = 
      store = false
    
    /*
     * print into out writer list of function's name with index.
     * 
     * ${index} ${name}\n
     * ...
     */
    override
    def print(out: Writer) = {
      for((index, name) <- for(i <- names.keySet.toSeq.sorted) yield (i, names(i)))
        out.append(s"${index} ${name}\n")
      out.flush()
    }
  }
}