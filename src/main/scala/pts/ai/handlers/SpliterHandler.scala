package pts.ai.handlers

import pts.ai.DefaultHandler
import java.io.Writer
import java.io.FileWriter
import java.io.BufferedWriter
import java.nio.file.Path
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

/** Spliter hanlder.
 *  
 *  Split mutliclass file in single class files.
 *  All class write in separate file with name of class and .txt extension
 * 
 * @dst - destinetion directory
 */
class SpliterHandler(private[this] val dst: Path) extends DefaultHandler {
  private var writer : BufferedWriter = null
  
  private var globals = new ArrayBuffer[String]
  
  private var order = new ArrayBuffer[String]
  
  private var descriptor : BufferedWriter = null
  
  override
  def global(name: String, value: String) =
    globals += s"${name} ${value}"
    
  override
  def classBegin(name: String, parent: String, `type`: Int) = {
    order += name
    writer = new BufferedWriter(new FileWriter(dst.resolve(s"${name}.txt").toFile))
    val p = 
      if(parent.equals("null"))
        "(null)"
      else
        parent
    writer.write(s"class ${`type`} ${name} : ${p}\n")
  }
  
  override
  def parameterBegin =
    writer.write("parameter_define_begin\n")
    
  override
  def parameter(name: String, `type`: String, value: String) = {
    writer.write(s"\t${`type`} ${name} ")
    if(`type`.equals("string")) {
      writer.write("\"") 
      writer.write(s"${value}")
      writer.write("\"\n")
    } else
      writer.write(s"${value}\n")
  }
    
  override
  def parameterEnd =
    writer.write("parameter_define_end\n")
  
  override
  def comment(value: String) = {
    commented = true
    writer.write(s"\t//${value}\n")
  }
        
  override
  def classEnd = {
    writer.write("class_end\n");
    writer.close()
    writer = null
  }
  
  override
  def handlerBegin(id: Int, lines: Int) =
    writer.write(s"handler ${id} ${lines}")

  override
  def handlerEnd = {
    if(!commented)
      writer.write("\n")
    commented = true
    writer.write("handler_end\n\n")
  }
  
  override
  def string(index: Int, value: String) = {
    if(!commented)
      writer.write("\n")
    commented = true
    writer.write(s"S${index}.\t")
    writer.write("\"")
    writer.write(value)
    writer.write("\"\n")
  }
  
  private var commented = false
  
  override
  def variableBegin =
    writer.write("\tvariable_begin\n")
    
  override
  def variable(name: String) = {
    writer.write("\t\t\"")
    writer.write(name)
    writer.write("\"\n")
  }
  
  override
  def variableEnd =
    writer.write("\tvariable_end\n\n")
    
  override
  def instruction(name: String, op: Array[String]) = {
    if(!commented)
      writer.write("\n")
    commented = false
    writer.write(s"\t${name}")
    if(op.length == 1)
      writer.write(s" ${op(0)}")
  }
  
  override
  def label(index: Int) = {
    if(!commented)
      writer.write("\n")
    commented = true
    writer.write(s"L${index}\n")
  }
  override
  def propertyBegin = 
    writer.write("property_define_begin\n")
  
  override
  def propertyEnd =
    writer.write("property_define_end\n")
    
    
  override
  def telposlistBegin(name: String) = {
    writer.write(s"\ttelposlist_begin ${name}\n")
  }
  
  override
  def telpos(data: Array[Any]) = {
    writer.write("\t\t{" + data.mkString("; ") + " }\n")  
  }
  
  override
  def telposlistEnd = {
    writer.write(s"\ttelposlist_end\n")
  }
  
  override
  def buyselllistBegin(name: String) =
    writer.write(s"\tbuyselllist_begin ${name}\n")
    
  override
  def item(data: Array[Any]) =
    writer.write("\t\t{" + data.mkString("; ") + " }\n")  
    
  override
  def buyselllistEnd =
    writer.write("\tbuyselllist_end\n")
 
  override
  def end = {
    {
      val writer = new BufferedWriter(new FileWriter(dst.resolve("__order.txt").toFile))
      writer.write(order.mkString("\n"))
      writer.flush
      writer.close
    }
    {
      val writer = new BufferedWriter(new FileWriter(dst.resolve("__globals.txt").toFile))
      writer.write(globals.mkString("\n"))
      writer.flush
      writer.close
    }
  }
  
}