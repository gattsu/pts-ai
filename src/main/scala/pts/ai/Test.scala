package pts.ai

import pts.ai.handlers.SpliterHandler
import java.nio.file.Paths
import pts.ai.handlers.Handlers

object Test {
  def main(args: Array[String]) = {
    //Parser("ai.obj", new SpliterHandler(Paths.get("original"))).parse
    //print all instruction
    val test = Handlers.instructions :: Handlers.hnames :: Handlers.fnames :: Nil
    Parser("ai.obj", test).parse
    test.foreach { _.print(Console.out) }  
  }
}


