package pts.ai

import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.attribute.FileAttribute
import pts.ai.handlers.SpliterHandler

class Application {
  private var src = Option.empty[String]
  
  private var dst = Option.empty[String]
  
  private var x = false
  
  def launch(args: Array[String]) = {
    if(args.length == 0)
      sys.error("Отсутствуют аргументы")
    scan(args.drop(1))
    args(0) match {
      case "help" => println(help)
      case "split" => {
        if(this.src.isEmpty)
          sys.error("Не указан файл источник. Используйте команду help для уточнения.")
        if(dst.isEmpty)
          sys.error("Не указана папка-назначения. Используйте команду help для уточнения.")
        val path = Paths.get(dst.get)
        if(!Files.exists(path)) {
          if(!x)
            sys.error(s"не было найдено папки-назначения ${path}")
          Files.createDirectories(path)
        }
        if(!Files.isDirectory(path))
           sys.error(s"Файл ${path.toAbsolutePath()} не является директорией")
        val src = Paths.get(this.src.get)
        if(!Files.exists(src))
          sys.error(s"Небыло найдено файла-источника ${src.toAbsolutePath()}")
        if(Files.isDirectory(src))
          sys.error(s"Файл-источник является директорией ${src.toAbsolutePath()}")
        Parser(src.toString, new SpliterHandler(path)).parse
      }
      case x => sys.error(s"Неизвестная комманда ${x}")
    }
  }
  
  @inline
  private[this]
  def scan(args: Array[String]) = {
    var it = args.iterator
    @inline
    def param : Option[String] = {
      if(it.hasNext) {
        val value = it.next
        if(value.startsWith("-"))
          sys.error("Ошибка формата параметра")
        Some(value)
      } else {
        sys.error("Не указано значение для параметра")
        None
      }
    }
    while(it.hasNext) {
      var value = it.next
      value match {
        case "--dst" => dst = param
        case "--src" => src = param
        case "-x" => x = true
      }
    }
  }

  private val help = List(
    "Комманды",
    " help",
    "  выводит на консоль, подробное описание, всех комманд.",
    "",
    " split --dst dst_dir --src src_file [-x]",
    "  Команда для разделения файла ai.obj, по файлам-классам.",
    "  Параметры: ",
    "   --dst папка назначения, куда будут сохраненны все файлы",
    "   --src файл источник, откуда извлекаются все классы",
    "  Флаги: ",
    "   -x - флаг указывает на создание папки-назначения, если её не существует, иначе отображается ошибка. Опционально."
  ).mkString("\n");

}

object Main extends Application {
  
  def main(args: Array[String]) : Unit =
    launch(args)

}