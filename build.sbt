name := "pts-ai"

version := "0.0.5"

scalaVersion  := "2.11.8"

crossPaths := false

javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

mainClass in (Compile, packageBin) := Some("pts.ai.Main")